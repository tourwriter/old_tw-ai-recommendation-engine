{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "\n",
    "import sqlite3\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import nltk\n",
    "import string\n",
    "from sklearn.feature_extraction.text import TfidfTransformer\n",
    "from sklearn.feature_extraction.text import TfidfVectorizer\n",
    "from sklearn.metrics.pairwise import cosine_similarity\n",
    "from sklearn.metrics.pairwise import linear_kernel\n",
    "from sklearn.feature_extraction.text import CountVectorizer\n",
    "import re\n",
    "from nltk.corpus import stopwords\n",
    "import pickle\n",
    "from tqdm import tqdm\n",
    "import os"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Reading the preprocessing file\n",
    "\n",
    "df = pd.read_pickle(\"textpreprocessing.plk\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Taking input from the traveller \n",
    "# 1) Country 2) Accomodation type 3) City\n",
    "\n",
    "def input_traveller(country,city,category):\n",
    "    \n",
    "    global meta_final\n",
    "    \n",
    "    #Taking the country input from the traveller\n",
    "    \n",
    "    meta = df.loc[df['Country'] == country]\n",
    "    \n",
    "    # Taking the city input from the traveller\n",
    "    meta1 = meta.loc[meta['City'] == city]\n",
    "    \n",
    "    # Taking the hotel category from the traveller\n",
    "    meta2 = meta1.loc[meta1['Category'] == category]\n",
    "    \n",
    "    # Filtering the hotel above rating 8\n",
    "    \n",
    "    meta_final = meta2.loc[meta2['Rating'] >= 8] \n",
    "    meta_final.reset_index(drop = True, inplace = True) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Recommending hotel accomodation \n",
    "def recommendations(hotel):\n",
    "    \n",
    "    # Converting the ingredients into vectors\n",
    "\n",
    "    tf = TfidfVectorizer(analyzer='word', ngram_range=(1, 3), min_df = 5, stop_words='english')\n",
    "    tfidf_matrix = tf.fit_transform(meta_final['final'])\n",
    "    \n",
    "    \n",
    "    cosine_similarities = cosine_similarity(tfidf_matrix, tfidf_matrix)\n",
    "    \n",
    "    #sig = sigmoid_kernel(tfidf_matrix, tfidf_matrix)\n",
    "    \n",
    "    hotels = meta_final[['Names']]\n",
    "    #Reverse mapping of the index\n",
    "    indices = pd.Series(meta_final.index, index = meta_final['Names'])\n",
    "       \n",
    "    idx = indices[hotel]\n",
    "    sim_scores = list(enumerate(cosine_similarities[idx]))\n",
    "    sim_scores = sorted(sim_scores, key = lambda x: x[1], reverse = True)\n",
    "    sim_scores = sim_scores[1:6]\n",
    "    hotel_indices = [i[0] for i in sim_scores]\n",
    "    return hotels.iloc[hotel_indices]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_traveller(\"New Zealand\", \"Auckland\", \"Hotel\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Names</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>skycity hotel auckland</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>36</th>\n",
       "      <td>crowne plaza auckland</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>19</th>\n",
       "      <td>amora hotel auckland</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>31</th>\n",
       "      <td>citylife auckland</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>14</th>\n",
       "      <td>m social auckland</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                     Names\n",
       "2   skycity hotel auckland\n",
       "36   crowne plaza auckland\n",
       "19    amora hotel auckland\n",
       "31       citylife auckland\n",
       "14       m social auckland"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "recommendations(\"president hotel auckland\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
