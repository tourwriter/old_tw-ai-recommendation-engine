{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "WARNING:tensorflow:From C:\\Users\\Dhilip\\Anaconda3\\lib\\site-packages\\tensorflow\\python\\compat\\v2_compat.py:96: disable_resource_variables (from tensorflow.python.ops.variable_scope) is deprecated and will be removed in a future version.\n",
      "Instructions for updating:\n",
      "non-resource variables are not supported in the long term\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "C:\\Users\\Dhilip\\Anaconda3\\lib\\site-packages\\ipykernel_launcher.py:13: FutureWarning: Passing a negative integer is deprecated in version 1.0 and will not be supported in future version. Instead, use None to not limit the column width.\n",
      "  del sys.path[0]\n"
     ]
    }
   ],
   "source": [
    "#import dependancies\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import re\n",
    "import tensorflow as tf\n",
    "import tensorflow_hub as tfhub\n",
    "import sqlite3\n",
    "from sqlite3 import Error\n",
    "import os\n",
    "import tensorflow.compat.v1 as tf\n",
    "tf.disable_v2_behavior()\n",
    "#force output to display the full description\n",
    "pd.set_option('display.max_colwidth', -1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reading the file\n",
    "df = pd.read_pickle(\"textpreprocessing.plk\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.environ[\"TFHUB_CACHE_DIR\"] = 'C:/dev'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Saver not created because there are no variables in the graph to restore\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Saver not created because there are no variables in the graph to restore\n"
     ]
    }
   ],
   "source": [
    "#create the tenserflow graph using placeholder to reduce overhead and time it takes to cache the hotel descriptions.\n",
    "#huge performance improvements doing it this way.\n",
    "\n",
    "g=tf.Graph()\n",
    "with g.as_default():\n",
    "    text_input = tf.placeholder(dtype = tf.string, shape=[None])\n",
    "    #embed = tfhub.Module(\"C:/Users/bendgame/Downloads/1fb57c3ffe1a38479233ee9853ddd7a8ac8a8c47\")\n",
    "    embed = tfhub.Module(\"https://tfhub.dev/google/universal-sentence-encoder/2\")\n",
    "    em_txt = embed(text_input)\n",
    "    init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])\n",
    "#g.finalize()\n",
    "\n",
    "session = tf.Session(graph = g)\n",
    "session.run(init_op)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Travellers input\n",
    "def input_traveller(country,city,category):\n",
    "    \n",
    "    global meta_final\n",
    "    \n",
    "    #Taking the country input from the traveller\n",
    "    \n",
    "    meta = df.loc[df['Country'] == country]\n",
    "    \n",
    "    # Taking the city input from the traveller\n",
    "    meta1 = meta.loc[meta['City'] == city]\n",
    "    \n",
    "    # Taking the hotel category from the traveller\n",
    "    meta2 = meta1.loc[meta1['Category'] == category]\n",
    "    \n",
    "    # Filtering the hotel above rating 8\n",
    "    \n",
    "    meta_final = meta2.loc[meta2['Rating'] >= 8] \n",
    "    meta_final.reset_index(drop = True, inplace = True) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_traveller('New Zealand', 'Auckland', 'Hotel')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0     ibis budget auckland airport                \n",
       "1     auckland rose park hotel                    \n",
       "2     skycity hotel auckland                      \n",
       "3     cordis auckland by langham hospitality group\n",
       "4     the grand by skycity                        \n",
       "5     president hotel auckland                    \n",
       "6     jet park airport hotel                      \n",
       "7     heritage auckland a heritage hotel          \n",
       "8     the spencer on byron hotel                  \n",
       "9     heartland hotel auckland airport            \n",
       "10    stamford plaza auckland                     \n",
       "11    greenlane suites                            \n",
       "12    leroy suites albany                         \n",
       "13    sofitel auckland viaduct harbour            \n",
       "14    m social auckland                           \n",
       "15    rydges auckland                             \n",
       "16    quality hotel lincoln green                 \n",
       "17    grand mercure auckland                      \n",
       "18    four points by sheraton auckland            \n",
       "19    amora hotel auckland                        \n",
       "20    waitakere resort  spa                       \n",
       "21    hilton auckland                             \n",
       "22    so auckland                                 \n",
       "23    the prospect of howick accommodation        \n",
       "24    hotel debrett                               \n",
       "25    the sebel auckland viaduct harbour          \n",
       "26    pullman auckland                            \n",
       "27    auckland budget inn                         \n",
       "28    the sebel auckland manukau                  \n",
       "29    hotel grand windsor mgallery by sofitel     \n",
       "30    novotel auckland ellerslie                  \n",
       "31    citylife auckland                           \n",
       "32    sudima auckland airport                     \n",
       "33    swissbelsuites victoria park auckland       \n",
       "34    ramada suites by wyndham manukau            \n",
       "35    ibis auckland ellerslie                     \n",
       "36    crowne plaza auckland                       \n",
       "37    grand millennium auckland                   \n",
       "38    grand central serviced apartments           \n",
       "39    novotel auckland airport                    \n",
       "40    holiday inn auckland airport                \n",
       "41    naumi auckland airport hotel                \n",
       "Name: Names, dtype: object"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "meta_final['Names']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Integrate and train our data into pretrained model\n",
    "result = session.run(em_txt, feed_dict={text_input:list(meta_final.final)})\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Recommendating the hotel\n",
    "\n",
    "def recommend_engine(query, embedding_table = result):\n",
    "    '''\n",
    "    takes user query, wine color, and embedded descriptions. Encodes the user query \n",
    "    and uses the dot product (calculated using numpy) to calculate the similarity \n",
    "    between the description and user query.\n",
    "    '''\n",
    "    \n",
    "    # Embed user query\n",
    "    with tf.Session(graph = g) as session:\n",
    "        session.run([tf.global_variables_initializer(), tf.tables_initializer()])\n",
    "        embedding = session.run(embed([query]))\n",
    "\n",
    "    # Calculate similarity with all reviews\n",
    "    similarity_score = np.dot(embedding, embedding_table.T)\n",
    "    \n",
    "    #used to calculate the cosine similarty \n",
    "    #     norm_a = np.linalg.norm(embedding)\n",
    "    #     norm_b = np.linalg.norm(embedding_table.T)\n",
    "    #     similarity_score = dot/(norm_a * norm_b)\n",
    "   \n",
    "    recommendations = meta_final.copy()\n",
    "    recommendations['recommendation'] = similarity_score.T\n",
    "    recommendations = recommendations.sort_values('recommendation', ascending=False)\n",
    "    recommendations = recommendations[['Names']][0:6]\n",
    "    return recommendations\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Saver not created because there are no variables in the graph to restore\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Saver not created because there are no variables in the graph to restore\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Names</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>30</th>\n",
       "      <td>novotel auckland ellerslie</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>27</th>\n",
       "      <td>auckland budget inn</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>35</th>\n",
       "      <td>ibis auckland ellerslie</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>auckland rose park hotel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>the spencer on byron hotel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>34</th>\n",
       "      <td>ramada suites by wyndham manukau</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                               Names\n",
       "30  novotel auckland ellerslie      \n",
       "27  auckland budget inn             \n",
       "35  ibis auckland ellerslie         \n",
       "1   auckland rose park hotel        \n",
       "8   the spencer on byron hotel      \n",
       "34  ramada suites by wyndham manukau"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "recommend_engine('sudima auckland airport')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Saver not created because there are no variables in the graph to restore\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Saver not created because there are no variables in the graph to restore\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Names</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>mercure wellington abel tasman hotel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>23</th>\n",
       "      <td>oaks wellington hotel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>14</th>\n",
       "      <td>sofitel wellington</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16</th>\n",
       "      <td>bay plaza hotel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>u boutique hotel</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>bolton hotel</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                                   Names\n",
       "1   mercure wellington abel tasman hotel\n",
       "23  oaks wellington hotel               \n",
       "14  sofitel wellington                  \n",
       "16  bay plaza hotel                     \n",
       "11  u boutique hotel                    \n",
       "9   bolton hotel                        "
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "recommend_engine('oaks wellington hotel')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
